package main
import ("fmt"; "strconv"; "reflect";"time";"sync";"runtime")

var K2 int=42         //scope Global  level start uppercase,exposed
var k3 int=42          //scope package level start lowercase

func part3_variables(){
    fmt.Println("part3")

    //variables
    fmt.Println("hello world")
    fmt.Println("42")
    var i int
    i=42
    var j int=42
    var p float32=42
    a :=42                       // scope block level
    k :=42                       // shadowing
    fmt.Printf("%v,%T \n",k,k)      // 42 , int
    l :=42.                        
    fmt.Println("%v,%T",l,l)      // 42 , float64 
    var ( u=1 ; v=2 )
    var outerVeryLongName int=1
    var innerShorter      int=1
    var t                 int=1
    fmt.Println(i,j,a,u,v,p,t,outerVeryLongName,innerShorter)
}    

func part4_primitives(){
    fmt.Println("part4")
    var i int=1
    var m float32=float32(42)
    var s string =string(42)
    fmt.Println(s)              // "*"
    s=strconv.Itoa(i)
    n,err :=strconv.Atoi(s)
    fmt.Println(i,m,s,err)
      
    var b bool=true
    h:= 1==1   //true
    var id int =0         //os default
    var i8 int8 =0
    var i16 int16 =0
    var i32 int32 =0
    var i64 int64 =0
    fmt.Println(b,n,h,id,i8,i16,i32,i64)
    
    var uid uint =0         //os default
    var ui8 uint8 =0
    var ui8b byte =0       //uint8
    var ui16 uint16 =0
    var ui32 uint32 =0
    fmt.Println(uid,ui8,ui8b,ui16,ui32)
    
    var f32 float32 =0
    var f64 float64 =0      
    fmt.Println(f32,f64)
    
    var c1 complex64=1+2i       //2 float64 
    var c2 complex128=complex(1,2)
    fmt.Println(c1,real(c1),imag(c1),c2)
    
    var st string ="alaindevos"
    var bb byte= st[2]
    fmt.Println(st,bb,st+st)
    
    // s5[2]=string(42) assignent is illegal
    // Conversion to bytes to manipulate strings
    var cb[] byte=[]byte(st)       
    fmt.Println(cb)       // ascii utf8 values
    var r1 int32 ='a'           // utf32 value
    var r2 rune ='a'           // utf32 value
    fmt.Println(r1,r2)       // ascii utf8 values
      
    //operators
    var b1 int  =3
    var b2 int8 =3
    var b3 int  =b1+int(b2)
    fmt.Println(b1,b2,b3)
    // & : bitwise and , |:bitwise or , ^:bitwise xor , <<: bitshifting
}      

func part5_constants(){
    fmt.Println("part5")

    //constants
    const myConst int = 42
    fmt.Println(myConst)
    //enumarated constants  ,iota: counter
    const ( a=iota ; b=iota ; c=iota)       
    fmt.Println(a,b,c)
}

func part6_arrays(){
    fmt.Println("part6")
    
    //arrays
    var ar1 [3]int= [3]int{1,2,3} 
    var ar2 [3]int= [...]int{1,2,3} 
    var ar3 [3]int
    ar3[0]=1          
    fmt.Println(ar1,ar2,ar3,len(ar3)) //ar3[1]=0,ar3[2]=0
    var idx [2][2]int=  [2][2]int { [2]int {1,0} , [2]int {0,1}}
    var idy [2][2]int=[...][2]int { [2]int {1,0} , [2]int {0,1}}
    var idz [2][2] int
    idz[0]=[...]int {1,0}
    idz[1]=[...]int {0,1}
    var idu [2][2]int
    idu=idz       // equal sign and functions calls copies arrays
    fmt.Println(idx,idy,idz,idu)
    
    //pointers
    idt:=&idu
    idu[0][0]=9             //changes idu and idt
    fmt.Println(idu,idt)
     
    //slice ,refence type
    s1:=[]int{1,2,3}
    s2:=s1
    s1[0]=9                // changes s1 and s2
    fmt.Println(s1,s2)
    
    s3:=s1[0:3]            //s1[0] ,s1[1], s1[2]           [first number is inclusive , second number is exlusive]
    s4:=s1[1:2]            //s1[1]
    fmt.Println(s3,s4)
    
    //convert array to slice
    sl1:=ar1[0:len(ar1)]
    fmt.Printf("%v, %T \n",ar1,ar1)  //[3]int 
    fmt.Printf("%v, %T \n",sl1,sl1)     //[]int
    sl2:=make([]int,3)               // makes a slice of length 3
    sl3:=make([]int,3,5)               // makes a slice of length 3 and capacity 5
    sl3=append(sl3,8,9)                    // length 5
    sl3=append(sl3,8,9)                    // length 7
    fmt.Println(sl2,sl3,len(sl3),cap(sl3)) // length 5 capacity 10(it doubles)
    
    sl4:=sl3[1:]      //removes first element
    sl5:=sl3[:len(sl4)] //removes last element   
    fmt.Println(sl4,sl5)
    
    //split element
    sl6:=[]int{1,2,3,4,5,6,7,8,9}
    fmt.Println(sl6)
    
    sl7:=sl6[:2]
    sl8:=sl6[2:]
    fmt.Println(sl6,sl7,sl8)
}

func part7_maps(){
    fmt.Println("part7")

    //maps collection type 
    //maps have no fixed order
    //data is passed by reference
    statepopulation:=map[string]int{
    "denderleeuw":1500,
    "aalst":3500,
    }
    map2:=make(map[string]int)
    statepopulation["ninove"]=300
    delete(statepopulation,"denderleeuw")
    fmt.Println(statepopulation,map2,len(statepopulation))
    pop,ok:=statepopulation["denderleeuw"]
    fmt.Println(pop,ok)
    
    //struct , datasets are copied
    type Doctor struct{
        number int
        name string 
        companions []string        
    }
    aDoctor:=Doctor{
        number:3,
        name:"Alain",
        companions: []string{"Eddy","Emiel"},
    }
    fmt.Println(aDoctor,aDoctor.companions[1])
    
    //anonymous struct , for short lived models
    aDoctor2:=struct{
            number int
            name string
            }   {
            number:16,
            name:"john",
            }
    bDoctor:=aDoctor  //copy
    cDoctor:=&aDoctor //pointer
    fmt.Println(aDoctor2,bDoctor,cDoctor)
    
    //embedding , for base behaviour, not for polymorphism, or interchanging object types
    type Animal struct{
        Name string
    }
    type Bird struct{
        Animal             // inner,type "inheritence",embedding  
        speed float32
    }
    bi1:=Bird{}
    bi1.Name="emu"
    bi1.speed=48
    bi2:=Bird{
        Animal: Animal{Name :"emu"},
        speed: 48,
    }
    fmt.Println(bi1,bi2)
    
    //tags
    type Animal2 struct{
        Name string `required max:"100"` //restrictions
    }
    atype:=reflect.TypeOf(Animal2{})
    afield,_:=atype.FieldByName("Name")
    atag:=afield.Tag
    fmt.Println(atag)     //prints : required max:"100"
}

func part8_if() {
     fmt.Println("part8")

    //Control Flow
    //if
    if true {
        fmt.Println("true")
        }
    
    //if else
    if ok:=(1==1)&&(0==0)&&(!false);ok {
        fmt.Println(ok)
        } else {
        fmt.Println("untrue")
        }
    
    //if else if 
    mycount:=3.5
    if mycount < 3{
        fmt.Println("mycount<3")
    } else if mycount < 4{
        fmt.Println("3<=mycount<4")
    } else {
        fmt.Println("4<=mycount")
    }
    
    //switch
    switch 3 {
        case 2:
                fmt.Println("2")
        case 3:
                fmt.Println("3")
        case 4:
                fmt.Println("4")
        default :
                fmt.Println("else")
    }
    
    //switch
    switch i:=1+1;i+1 {
        case 1,3,5:
                fmt.Println("odd")
        case 2,4,6:
                fmt.Println("even")
        default :
                fmt.Println("unknown")
    }
    
    //switch with implicit break between cases
    ii:=3.5
    switch {
        case ii<3:
                fmt.Println("ii<3")
        case (3<=ii) && (ii<=4):
                fmt.Println("3<=ii<=4")
        case ii>4:
                fmt.Println("ii>4")
        default:
    }
    
    //fallthrough
    switch {
        case ii<3:
                fmt.Println("ii<3")
                fallthrough 
        case ii<4:
                fmt.Println("ii<4")
                fallthrough 
        case ii<5:
                fmt.Println("ii<5")
                fallthrough 
        default:
    }
    
    //An interface can be any type , here int 
    //switch of type
    var ty interface{} = 1
    switch ty.(type) {
        case int:
            fmt.Println("ty has type int")
        case float64:
            fmt.Println("ty has type float64")
            break
            fmt.Println("do not execute this")
        default: 
    }
}


func part9_looping() {
    fmt.Println("part9")

    //For looping
    for a:=1;a<=3;a++ {
        fmt.Println(a)
    }
    
    a:=1
    for ;a<=3;a++ {
        fmt.Println(a)
    }
    
    a=1
    //while 
    for ;a<=3;{
        fmt.Println(a)
        a++
    }
    
    //while with sugar
    a=1
    for a<=3{
        fmt.Println(a)
        a++
    }
    
    // do while
    a=1
    for {
        fmt.Println(a)
        a++
        if a==4 {
            break 
        }
    }
    
    // continue
    a=1
    for{
        fmt.Println(a)
        a++
        if a<4{
            continue
        }
    break
    }
    
    //Label to break out of nested loops
    Myouter:
    for i1:=0;i1<3;i1++ {
        for i2:=0;i2<3;i2++ {
            i3:=i1*i2;
            fmt.Println(i3)
            if (i3>5) {
                break Myouter 
            }
        }
    }
    
    //Collection for loops
    //slice 
    sl:= []int{3,2,1}
    for k,v:=range sl {
        //k is key or index ; v is the value
        fmt.Println(k,v)
    }
    
    //array     
    //slice 
    ar:= [3]int{3,2,1}
    for k,v:=range ar {
        //k is key or index ; v is the value
        fmt.Println(k,v)
    }
    
    //map
    mapa:=map[int]int {
        0: 3,
        1: 2,
        2: 1,
    }
    for k,v:=range mapa {
        //k is key or index ; v is the value
        fmt.Println(k,v)
    }
    
    s:="321"
    for k,v:=range s {
        //k is key or index ; v is the value
        fmt.Println(k,string(v))
    }
    // _ : dummy or placeholder
    s="321"
    for _,v:=range s {
        //k is key or index ; v is the value
        fmt.Println(string(v))
    }
}

func part10_defer() {
    fmt.Println("part10")

    //defer , order  is lifo , used to close resources
    // E.g. file.open
    //      defer file.close
    {
        fmt.Println("a")
        defer fmt.Println("1")
        fmt.Println("b")        
    }
    
    {
        defer fmt.Println("2")
        fmt.Println("a")
        defer fmt.Println("3")
        fmt.Println("b")        
    }
    
    //Prints "start"
    {
        fmt.Println("A:-------------")
        a:="4:start"
        defer fmt.Println(a)
        a="4:end"
    }
    
    //Panics (exceptions) ,used when an error returned must be handled.
    //Note: Panics happen after defer statements are executed.
    //Panics don't have to be fatal
    {
        fmt.Println("B:------------")
        fmt.Println("Take resource")
        defer fmt.Println("5:Release resource")
        a,b:=1,0.1
        if (b==0){
                    panic("runtime error: integer divide by zero Byebye")
        }
        fmt.Println(a,b)
//      ans:=a/b 
//      fmt.Println(ans)
    }
    fmt.Println("--------------")
    fmt.Println("Deferred stuff")
}


func part11_pointers() {
    fmt.Println("part11")

    //Pointers
    var a   int = 42
    var pa *int = &a        //address
    fmt.Println(pa)         //address 
    fmt.Println(*pa)        //dereference operator, value 
    *pa=7
    fmt.Println(*pa)        //dereference operator, value 
    
    var ar=[3]int{1,2,3}
    p0:=&ar[0]
    p1:=&ar[1]
//  p2:=&ar[1]+8            //invalid
    fmt.Printf("%v %p %p \n",ar,p0,p1)
    
    type aStruct struct{
        aint int
    }
    var ps *aStruct
    ps=&aStruct{ aint: 42}
    fmt.Println(ps)
    
    var ps2 *aStruct       
    fmt.Println(ps2)               // <nil>
    ps2=new(aStruct)
    fmt.Println(ps2)               // &{0}
    (*ps2).aint=5
    fmt.Println(ps2)               // &{5}
    fmt.Println((*ps2).aint)       // 5
    fmt.Println(ps2.aint)          // sugar
    
    //array ,copies
    ar=[3] int{1,2,3}
    ar2:=ar                     //copies 
    fmt.Println(ar,ar2)
    ar[1]=42
    fmt.Println(ar,ar2)

    //slice , by reference 
    var sl=[] int{1,2,3}
    sl2:=sl                     // 
    fmt.Println(sl,sl2)
    sl[1]=42                    //changes both
    fmt.Println(sl,sl2)
    fmt.Printf("%p %p \n",&sl,&sl2)
    fmt.Printf("%p %p \n",&sl[1],&sl2[1])      //same
    
    //map , by reference 
    ma:=map[string]string{"aaa":"111","bbb":"222"}
    ma2:=ma
    fmt.Println(ma,ma2)
    ma["bbb"]="333"            //changes both
    fmt.Println(ma,ma2)
    
    //Be carefull passing maps and slices in an application can change underlying data
    //This does not happen with primitives,arrays,structs [There normally everything is copied]
    
}

//functions
func myFunction1(msg string,idx int) {
        fmt.Println(msg,idx)
}
//Same types
func myFunction2(msg1,msg2 string) {
        fmt.Println(msg1,msg2)
}

//Pointers
func myFunction3(msg *string) {
        fmt.Println(*msg)
        *msg="1234567"
}

func mySum(msg string,values ...int){
    fmt.Println(values)
    fmt.Printf("%T \n",values)        //slice
    result:=0
    for _,v:=range values{
        result +=v
    }
    fmt.Println(msg,result)
}

func mySum2(values ...int) int {
    result:=0
    for _,v:=range values{
        result +=v
    }
    return result
}

func mySum3(values ...int) *int {
    result:=0
    for _,v:=range values{
        result +=v
    }
    return &result                   // the memory is moved from local stack to heap memory
}

//named variables ,syntactical sugar
func mySum4(values ...int) (myresult int) {
    myresult=0
    for _,v:=range values{
        myresult +=v
    }
    return 
}

//multiple return values and error type
func divide(t,n int)(int,error) {
//error checking at beginning
    if n==0 {
        return 0,fmt.Errorf("Cannot divide by zero")
    } else {
        return (t/n),nil
    }    
}

func part12_alpha() {
    fmt.Println("part12_alpha")

    //two parameters
    myFunction1("Hallo",2)
    
    //same types
    myFunction2("Hallo","Daar")
    
    //Pass by reference ,is faster
    astring:="Alain"
    myFunction3(&astring)
    fmt.Println(astring)             //1234567
    
    //variable arguments
    mySum("The sum is :",1,2,3)
    s:=mySum2(1,2,3)
    fmt.Println(s)
    
    var s2 *int=mySum3(1,2,3)
    fmt.Println("The sum is",*s2)

    fmt.Println(mySum4(1,2,3))
    
    var d int
    var err error
    d,err=divide(10,0)
    fmt.Println(d,err)
    if(err!=nil){
        fmt.Println(err)
    } else {
        fmt.Println(d)
    }
    
    //declare an anonymous function i.e. without a name
    func () {
        msg:="Hallo"              //isolated scope
        fmt.Println(msg) 
    } ()  //invoke immediately
    
    //with loop 
    for i:=0;i<3;i++ {
        func (j int) {
            fmt.Println(j) 
        } (i)  //invoke immediately
    }
    
    //anonymous function as variable
    k:=func(){
        fmt.Println("hi")
    }
    k()          //invoke
    
    var maal func(int,int) (int)
    maal=func(a int,b int)(c int){
        c=a*b
        return 
    }
    fmt.Println(maal(2,3))
    
    
}

//methods
type greeter struct {
    greeting string
    name string
}

//methods , functions acting on context (any type,eg structs)
//here the struct greeter is copied and we can access the fields
//we cannot access the struct itself
func (g greeter) greet(){
    fmt.Println(g.greeting,g.name)
    g.name="Eddy"                         //no effect
}

//pointer receiver
func (g *greeter) greet2(){
    fmt.Println(g.greeting,g.name)
    g.name="Emiel"                         //with effect
}

func part12_beta(){
    fmt.Println("part12_beta")

    h:=greeter {
        greeting: "Hello",
        name:"Alain",
    }
    //methods , functions acting on an value,here greeter 
    h.greet()            //Alain
    h.greet()            //Alain
    h.greet2()           //Alain
    h.greet2()           //Emiel
}

func part12_functions() {
    part12_alpha()
    part12_beta()
}

//definition , it takes a slice of bytes and write to something returning bytes written
//naming convention is method name, here Write + er = Writer
type Writer interface {
    Write([]byte) (int,error)
}

//implicit implement , a struct 
type ConsoleWriter struct {}

//Print to console 
func (cw ConsoleWriter) Write(data []byte)(int,error){
    n,err := fmt.Println(string(data))
    return n,err 
}

func part13_writer() {
    fmt.Println("part13_writer")

// interfaces store behaviours, method definitions
// w is something that implements the writer interface 
    var w Writer=ConsoleWriter{}
// Polymorphic behaviour, w could be TcpWriter ...
    w.Write([]byte("Interface "))
}


type Incrementer interface {
    Increment() int 
}

//type alias
type IntCounter int

//implementation for the interface 
func (ic *IntCounter) Increment() int {
    (*ic)=(*ic)+1
    return int(*ic)
}

func part13_incrementer(){
    fmt.Println("part13_incrementer")

    myInt:=IntCounter(0)
    var inc Incrementer=&myInt
    for i:=0;i<3;i++ {
        fmt.Println(inc.Increment())
    }
}    

type Writer2 interface {
    Write([]byte) (int,error)
    
}

type Closer2 interface {
    Close() error
}

type WriterCloser2 interface{
    Writer2
    Closer2 
}

type myWriterCloser2 struct {
}

func(mwc myWriterCloser2) Write(data[] byte) (int,error){
    return 0,nil
}

func(mwc myWriterCloser2) Close() error{
    return nil
}

// //        {........}
// Constructor
// func NewconsoleWriterCloser() *consoleWriterCloser
//        {........}

func part13_compose(){
    fmt.Println("part13_compose")

    // all func( X myWriterCloser2)
    var wc WriterCloser2=myWriterCloser2{}
    wc.Write([]byte("Hallo"))
    wc.Close()
    fmt.Println(wc)

    // all func( X myWriterCloser2) and func( X *myWriterCloser2)
    var pwc WriterCloser2=&myWriterCloser2{}
    pwc.Write([]byte("Hallo"))
    pwc.Close()
    fmt.Println(pwc)
    
//INTERFACE_CONVERSION_TO_ACCESS_INTERNAL_STRUCTURE
//  wc3:=wc.(*consoleWriterCloser)
}

func part13_interfaces() {
    part13_writer()
    part13_incrementer()
    part13_compose()
    
    //empty interface , an interface with no methods, usefull for "type conversion"
    var ei interface{}
    
    ei=0
    switch ei.(type){
        case string:
            fmt.Println("ei is a string")
        case int:
            fmt.Println("ei is an integer")
    }
    
    ei="alain"
    switch ei.(type){
        case string:
            fmt.Println("ei is a string")
        case int:
            fmt.Println("ei is an integer")
    }
}

func sayHello(){
    fmt.Println("Hello")
}

// Concurrent en parallel programming , use of many cors
func part14_alfa() {
    fmt.Println("part14_alpha")

    //spin greenthread , no os thread but lightweigth thread 
    // go routines are mapped to os threads , small stack spaces
    go sayHello()                //spawned thread 
    time.Sleep(100*time.Millisecond)
    
    //anonymous
    var msg="Hallo"
    go func() {
        time.Sleep(10*time.Millisecond)
        fmt.Println(msg)   //prints goodbye ,race, bad idea
    }()
    
    msg="Goodbye"
    time.Sleep(100*time.Millisecond)

    msg="Hallo"
    go func(msg string) {
        time.Sleep(10*time.Millisecond)
        fmt.Println(msg)   //prints hallo ,ok , a copy of msg 
    }(msg)
    msg="Goodbye"
    time.Sleep(100*time.Millisecond)
 }

var wgb=sync.WaitGroup{}            // object of type waitgroup {}

func part14_beta() {
    fmt.Println("part14_beta")

    msg:="Hallo\n"
    wgb.Add(1)              //add 1
    go func(msg string) {
        fmt.Println(msg)   //prints hallo ,ok , a copy of msg
        wgb.Done()          // substract 1
    }(msg)
    msg="Goodbye\n"
    wgb.Wait()              //wait until 0
    msg="Gedaan\n"
}

var wg=sync.WaitGroup{}     //global
var counter=0
// lock, protects code
// readwrite mutex, 
//      - maximum one write at the same time
//      - if one is reading no one can write
//      - as many as possible read at the same time
var m=sync.RWMutex{}        

func sayHellog (){
    fmt.Printf("Hello #%v\n",counter)
    m.RUnlock()                        //Unlocks are done asynchronously
    wg.Done()                          // decrease the number of threads
}

func increment(){
    counter++
    m.Unlock()                        //Unlocks are done asynchronously
    wg.Done()                         //decrease the number of threads
}

func part14_gamma(){
    fmt.Println("part14_gamma")

    fmt.Printf("Threads available: %v \n",runtime.GOMAXPROCS(-1))         //8 os threads because of 8 cores
//    runtime.GOMAXPROCS(1)           // eg to avoid race conditions
    runtime.GOMAXPROCS(4)             // to map all go threads to 4 os threads     
    for i:=0;i<5;i++{
        wg.Add(1)                     // increase the number of threads
        m.RLock()                     // Locks are done in the main thread
        go sayHellog()
        wg.Add(1)                     // increase the number of threads
        m.Lock()                      // Locks are done in the main thread    
        go increment()
    }
    wg.Wait()                         // wait until all threads done
}


func part14_goroutines() {
    part14_alfa()
    part14_beta()
    part14_gamma()
}

var wgc=sync.WaitGroup{}

//to synchronise data between different go routines (threads)
func part15_alfa(){
    fmt.Println("part15_alpha")

    //a channel to send integers
    ch:=make(chan int)         //int,datatype that flows through the channel
    //consumer of channel
    wgc.Add(1)                  
    go func() {
        i:= <- ch
        fmt.Println(i)
        i=1111                 //changes the copy
        wgc.Done()              
    }()
    //provider of channel
    wgc.Add(1)                  
    go func() {
        ch <- 42                // copies the 42 before sending , arrow show direction of data flow
        wgc.Done()              
    }()
    //Wait until both done
    wgc.Wait()                 
}

func part15_beta(){
    fmt.Println("part15_beta")
    //a channel to send integers
    ch:=make(chan int)
    
    for j:=0;j<5;j++ {
        //consumer of channel
        wgc.Add(1)                  
        go func() {
            i:= <- ch
            fmt.Println(i)
            i=1111                 //changes the copy
            wgc.Done()              
        }()
        //provider of channel
        wgc.Add(1)                  
        go func() {
            // copies the 42 before sending , arrow show direction of data flow
            // Note it block until the channel is free, the channel is unbuffered
            ch <- 42                
            wgc.Done()              
        }()
    }
    //Wait until both done
    wgc.Wait()
}

// readwrite channel
func part15_gamma(){
    fmt.Println("part15_gamma")
    //a channel to send integers
    ch:=make(chan string)
    
    for j:=0;j<5;j++ {
        wgc.Add(1)                  
        go func() {
            ina:= <- ch
            fmt.Println(ina)
            ch <- "Second"
            wgc.Done()              
        }()
        wgc.Add(1)                  
        go func() {
            ch <- "First" 
            inb:= <- ch
            fmt.Println(inb)
            wgc.Done()              
        }()
    }
    //Wait until both done
    wgc.Wait()
}

//go routines, undirectional 
func part15_delta(){
    fmt.Println("part15_delta")
    //a channel to send integers
    ch:=make(chan string)
    
    wgc.Add(1)
    //receive only channel
    go func(ch <- chan string) {       //cast of a bidirectional channel to a uniderectional
        ina:= <- ch
        fmt.Println(ina)
        //ch <- "Second" ,error
        wgc.Done()              
    }(ch)
    wgc.Add(1)
    //send only channel 
    go func(ch chan<- string) {
        ch <- "First" 
        //inb:= <- ch ,error
        //fmt.Println(inb)
        wgc.Done()              
    }(ch)
    //Wait until both done
    wgc.Wait()
}

//buffered channels , if sender and receiver have other frequency
func part15_epsilon(){
    fmt.Println("part15_epsilon")
    //a channel to receive 50 string
    ch:=make(chan string,50)
    
    wgc.Add(1)
    //receive only channel
    go func(ch <- chan string) {       //cast of a bidirectional channel to a uniderectional
        for true {
            i,ok := <- ch

            fmt.Printf("i: Type : %T, value : %v \n",i,i)
            fmt.Printf("ok: Type : %T, value : %v \n",ok,ok)

            if ! ok {
                break
            }

            fmt.Println("Data:",i)
        }
        wgc.Done()              
    }(ch)
    wgc.Add(1)
    //send only channel 
    go func(ch chan<- string) {
        ch <- "First" 
        ch <- "Second"
        close(ch)
        wgc.Done()              
    }(ch)
    //Wait until both done
    wgc.Wait()
}


type logEntry struct {
    time time.Time
    severity string
    message string
}

var logCh=make(chan logEntry,50)
var doneCh=make(chan struct{})         // struct with no fields , signal only channel

func logger(){
    for true {
        select { //listen to two channels
            case entry:= <- logCh:
                fmt.Printf("%v - [%v] %v \n",entry.time,entry.severity,entry.message)
            case <-doneCh:
                fmt.Println("T'is gedaan")
                break
        //  default:         // for nonblocking select
        } // select 
    }//for
}//func

const (
    alogInfo string = "INFO" ;
    alogWarning string = "WARNING"    
    alogError string = "ERROR"
)

//E.g :  logch:=make(chan string,50)
// select, listen to two channels, is normally blocking , it waits until something received
// For nonblocking add "default:" case
func part15_zeta(){
    go logger()
//Note : when you create a go routine, you should have a strategy on how to shut down this routine
//  defer func(){
//  close(logch)
//  }()
    logCh <- logEntry{time.Now(),alogInfo,"App is starting"}
    logCh <- logEntry{time.Now(),alogInfo,"App is shutting down"}
    doneCh <-struct{}{}          //struct{} initialised with {}
    time.Sleep(100*time.Millisecond)
}


func part15_channels() {
    part15_alfa()
    part15_beta()
    part15_gamma()
    part15_delta()
    part15_epsilon()
    part15_zeta()
}


func main(){
    part3_variables()
    part4_primitives()
    part5_constants()
    part6_arrays()
    part7_maps()
    part8_if()
    part9_looping()
    part10_defer()
    part11_pointers()
    part12_functions()
    part13_interfaces()
    part14_goroutines()
    part15_channels()

}